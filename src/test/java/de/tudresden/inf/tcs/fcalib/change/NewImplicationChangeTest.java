package de.tudresden.inf.tcs.fcalib.change;

import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import static org.junit.Assert.*;


public class NewImplicationChangeTest {

	@Test
	public void testNewImplicationChange_1()
		throws Exception {
		Context c = new FormalContext();
		FCAImplication<Object> i = new Implication();
		NewImplicationChange result = new NewImplicationChange(c, i);
		c=null;
		i=null;
		assertNotNull(result);
		assertEquals(1, result.getType());
	}

	@Test
	public void testNewImplicationChange_2()
		throws Exception {
		Context c = new FormalContext();
		FCAImplication<Object> i = new Implication();
		NewImplicationChange result = new NewImplicationChange(c, i);
		assertNotNull(result);
		assertEquals(1, result.getType());
	}
	@Test
	public void testGetImplication_1()
		throws Exception {
		NewImplicationChange fixture = new NewImplicationChange(new FormalContext(), new Implication());

		FCAImplication<Object> result = fixture.getImplication();
		assertNotNull(result);
	}

	@Test
	public void testGetType_1()
		throws Exception {
		NewImplicationChange fixture = new NewImplicationChange(new FormalContext(), new Implication());

		int result = fixture.getType();
		assertEquals(1, result);
	}


}