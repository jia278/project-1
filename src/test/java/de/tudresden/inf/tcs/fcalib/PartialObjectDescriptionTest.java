package de.tudresden.inf.tcs.fcalib;

import java.util.HashSet;
import java.util.Set;
import org.junit.*;
import static org.junit.Assert.*;


public class PartialObjectDescriptionTest {

	@Test
	public void testPartialObjectDescription_1()
		throws Exception {
		Set<Object> p = new HashSet();

		PartialObjectDescription result = new PartialObjectDescription(p);
		assertNotNull(result);
		assertEquals("plus=[] minus=[]", result.toString());
	}
	
	@Test
	public void testPartialObjectDescription_2()
		throws Exception {
		Set<Object> p = new HashSet();
		Set<Object> m = new HashSet();

		PartialObjectDescription result = new PartialObjectDescription(p, m);
		assertNotNull(result);
		assertEquals("plus=[] minus=[]", result.toString());
	}

	@Test
	public void testAddAttribute_1()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		boolean result = fixture.addAttribute(null);
		assertEquals(true, result);
		
	}
	
	
	@Test
	public void testAddAttribute_2()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		boolean result = fixture.addAttribute("");
		assertEquals(true, result);
		
	}
	
	@Test 
	public void testAddAttribute_3()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		fixture.addAttribute(1);
		boolean result = fixture.addAttribute(1);
		assertEquals(false, result);
		
	}
	
	@Test
	public void testAddNegatedAttribute_1()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		boolean result = fixture.addNegatedAttribute(null);
		assertEquals(true, result);
	}
	
	@Test
	public void testAddNegatedAttribute_2()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		fixture.addNegatedAttribute(1);
		boolean result = fixture.addNegatedAttribute(1);
		assertEquals(false, result);
	}
	
	@Test (expected = Exception.class)
	public void testAddNegatedAttribute_3()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		fixture.addAttribute("");
		boolean result = fixture.addNegatedAttribute("");

	}
	
	@Test
	public void testContainsNegatedAttribute_1()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		boolean result = fixture.containsNegatedAttribute(null);
		assertEquals(false, result);
	}
	
	@Test
	public void testContainsNegatedAttribute_2()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		fixture.containsNegatedAttribute(1);
		boolean result = fixture.containsNegatedAttribute(1);
		assertEquals(false, result);
	}
	
	@Test
	public void testContainsNegatedAttribute_3()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		fixture.containsNegatedAttribute("");
		boolean result = fixture.containsNegatedAttribute("");
		assertEquals(false, result);
	}
	
	@Test (expected = Exception.class)
	public void testContainsNegatedAttributes_1()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		Set<Object> s = new HashSet();
		fixture.containsNegatedAttribute(s);
		s=null;
		boolean result = fixture.containsNegatedAttributes(null);
	}
	
	@Test
	public void testContainsNegatedAttributes_2()
			throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		Set<Object> s = new HashSet();
		boolean result = fixture.containsNegatedAttributes(s);
		assertEquals(true, result);
	}
	@Test
	public void testClone()
		throws Exception {
		PartialObjectDescription fixture = new PartialObjectDescription();
		fixture.clone();
	}

}