package de.tudresden.inf.tcs.fcalib;

import java.util.HashSet;
import de.tudresden.inf.tcs.fcaapi.Expert;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.test.NoExpertFull;
import java.util.Set;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import org.junit.*;
import static org.junit.Assert.*;


public class PartialContextTest {

	@Test
	public void testPartialContext_1()
		throws Exception {

		PartialContext result = new PartialContext();
		assertNotNull(result);
		assertEquals(null, result.getExpert());
		assertEquals(null, result.getStemBase());
		assertEquals(0, result.getObjectCount());
		assertEquals(null, result.getDuquenneGuiguesBase());
		assertEquals(0, result.getAttributeCount());
		assertEquals(null, result.getImplications());
		assertEquals(null, result.getCurrentQuestion());
		assertEquals(false, result.isExpertSet());
	}


	@Test (expected = Exception.class)
	public void testAddAttributeToObject_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.addAttributeToObject(null, null);

	}

	
	@Test (expected = Exception.class)
	public void testAddAttributeToObject_2()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.addAttributeToObject(null, 1);
	}


	@Test (expected = Exception.class)
	public void testAddAttributeToObject_3()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.addAttributeToObject(1, null);

	}

	@Test (expected = Exception.class)
	public void testAddAttributeToObject_4()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.addAttributeToObject("","");

	}


	@Test (expected = Exception.class)
	public void testAddAttributeToObject_5()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.addAttributeToObject("car", 1);
	}


	@Test (expected = Exception.class)
	public void testAddObject_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.addObject(null);

	}


	@Test
	public void testClearObjects_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		fixture.clearObjects();
	}


	@Test
	public void testDoublePrime_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		Set<Object> x = new HashSet();
		x=null;
		Set<Object> result = fixture.doublePrime(x);
		assertNotNull(result);
		assertEquals(0, result.size());
	}


	@Test
	public void testDoublePrime_2()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		Set<Object> x = new HashSet();
		Set<Object> result = fixture.doublePrime(x);
		assertNotNull(result);
		assertEquals(0, result.size());
	}


	@Test
	public void testFollowsFromBackgroundKnowledge_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		FCAImplication<Object> implication = new Implication();

		boolean result = fixture.followsFromBackgroundKnowledge(implication);

		assertEquals(false, result);
	}


	@Test
	public void testGetDuquenneGuiguesBase_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		Set<FCAImplication<Object>> result = fixture.getDuquenneGuiguesBase();

		assertEquals(null, result);
	}


	@Test
	public void testGetObject_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		PartialObject result = fixture.getObject(null);
		assertEquals(null, result);
	}


	@Test
	public void testGetObject_2()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		PartialObject result = fixture.getObject(1);

		assertEquals(null, result);
	}


	@Test
	public void testGetObject_3()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		PartialObject result = fixture.getObject("");
		assertEquals(null, result);
	}


	@Test (expected = Exception.class)
	public void testGetObjectAtIndex_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		int index = -1;
		PartialObject result = fixture.getObjectAtIndex(index);
		assertNotNull(result);
	}

	@Test  (expected = Exception.class)
	public void testGetObjectAtIndex_2()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		int index = 1;

		PartialObject result = fixture.getObjectAtIndex(index);
		assertNotNull(result);
	}
	
	@Test  (expected = Exception.class)
	public void testGetObjectAtIndex_3()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		int index = 0;

		PartialObject result = fixture.getObjectAtIndex(index);
		assertNotNull(result);
	}

	@Test
	public void testGetObjectCount_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		int result = fixture.getObjectCount();

		assertEquals(0, result);
	}

	@Test
	public void testGetObjects_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		IndexedSet<PartialObject> result = fixture.getObjects();

		assertNotNull(result);
		assertEquals(0, result.size());
	}


	@Test
	public void testGetStemBase_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		Set<FCAImplication<Object>> result = fixture.getStemBase();

		assertEquals(null, result);
	}


	@Test   (expected = Exception.class)
	public void testIsCounterExampleValid_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		FCAImplication<Object> imp = new Implication();
		imp=null;
		boolean result = fixture.isCounterExampleValid(null, imp);
	}


	@Test (expected = Exception.class)
	public void testIsCounterExampleValid_2()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		FCAImplication<Object> imp = new Implication();

		boolean result = fixture.isCounterExampleValid(null, imp);
		assertTrue(result);
	}


	@Test (expected = Exception.class)
	public void testObjectHasAttribute_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.objectHasAttribute(null, null);

	}


	@Test (expected = Exception.class)
	public void testObjectHasNegatedAttribute_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.objectHasNegatedAttribute(null, null);
		assertTrue(result);
	}


	@Test
	public void testRefutes_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		FCAImplication<Object> imp = new Implication();
		imp=null;
		boolean result = fixture.refutes(imp);
		assertEquals(false, result);
	}


	@Test
	public void testRefutes_2()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		FCAImplication<Object> imp = new Implication();

		boolean result = fixture.refutes(imp);
		assertEquals(false, result);
	}

	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.removeAttributeFromObject(null, null);
	}

	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_2()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.removeAttributeFromObject(1, null);
	}


	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_3()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.removeAttributeFromObject(null, "");

	}

	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_4()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.removeAttributeFromObject(1, 1);
		assertTrue(result);
	}

	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_5()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.removeAttributeFromObject("car", 6);

		assertTrue(result);
	}


	@Test  (expected = Exception.class)
	public void testRemoveObject_1()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.removeObject(null);
	}


	@Test  (expected = Exception.class)
	public void testRemoveObject_2()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.removeObject(1);
	}


	@Test  (expected = Exception.class)
	public void testRemoveObject_3()
		throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		boolean result = fixture.removeObject("");
	}
	@Test(expected=Exception.class)
	public void testAddAttributeToObject_6()
			throws Exception {
			PartialContext fixture = new PartialContext();
			fixture.setExpert(new NoExpertFull(new FormalContext()));
			fixture.addObject(new PartialObject(2));
			Object a = 1;
			fixture.addAttribute(a);
			fixture.addAttributeToObject(a, 1);
		}
	@Test(expected=Exception.class)
	public void testAddAttributeToObject_7()
			throws Exception {
			PartialContext fixture = new PartialContext();
			fixture.setExpert(new NoExpertFull(new FormalContext()));
			fixture.addObject(new PartialObject(1));
			Object a = 1;
			fixture.addAttribute(a);
			fixture.addAttributeToObject(a, 1);
			fixture.addAttributeToObject(a, 1);

		}
	@Test
	public void testRemoveAttributeToObject_5()
			throws Exception {
			PartialContext fixture = new PartialContext();
			fixture.setExpert(new NoExpertFull(new FormalContext()));
			fixture.addObject(new PartialObject(1));
			Object a = 1;
			fixture.addAttribute(a);
			fixture.addAttributeToObject(a, 1);
			fixture.removeAttributeFromObject(a, 1);
		}
	@Test(expected=Exception.class)
	public void testRemoveAttributeToObject_4()
			throws Exception {
			PartialContext fixture = new PartialContext();
			fixture.setExpert(new NoExpertFull(new FormalContext()));
			fixture.addObject(new PartialObject(1));
			Object a = 1;
			fixture.addAttribute(a);
			fixture.addAttributeToObject(a, 1);
			fixture.removeAttributeFromObject(a, 2);
		}
	@Test(expected=Exception.class)
	public void testRemoveAttributeToObject_3()
			throws Exception {
			PartialContext fixture = new PartialContext();
			fixture.setExpert(new NoExpertFull(new FormalContext()));
			fixture.addObject(new PartialObject(1));
			Object a = 1;
			Object b = 2;
			fixture.addAttribute(a);
			fixture.addAttribute(b);

			fixture.addAttributeToObject(a, 1);
			fixture.removeAttributeFromObject(b, 1);
		}
	@Test
	public void testRemoveObject()
			throws Exception {
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new PartialObject(1));
		fixture.removeObject(1);
	}
	@Test
	public void testObjectHasAttribute()
		throws Exception{
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		PartialObject o = new PartialObject(1);
		fixture.addObject(o);
		Object a = 1;
		fixture.addAttribute(a);
		fixture.addAttributeToObject(a, 1);
		fixture.objectHasAttribute(o, a);
	}
	@Test
	public void testObjectHasNegatedAttribute()
		throws Exception{
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		PartialObject o = new PartialObject(1);
		fixture.addObject(o);
		Object a = 1;
		fixture.addAttribute(a);
		fixture.addAttributeToObject(a, 1);
		fixture.objectHasNegatedAttribute(o, a);
	}
	@Test
	public void testAddObject()
		throws Exception{
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		PartialObject o = new PartialObject(1);
		fixture.addObject(o);
		fixture.addObject(o);
	}
	@Test
	public void testRemoveObject_4()
		throws Exception{
		PartialContext fixture = new PartialContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		PartialObject o = new PartialObject(1);
		fixture.addObject(o);
		fixture.removeObject(o);
		
	}
}