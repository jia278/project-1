package de.tudresden.inf.tcs.fcalib;

import java.util.HashSet;
import java.util.Set;
import org.junit.*;
import static org.junit.Assert.*;


public class FullObjectDescriptionTest {

	
	@Test
	public void testFullObjectDescription_1()
		throws Exception {
		Set<Object> p = new HashSet();

		FullObjectDescription result = new FullObjectDescription(p);
		assertNotNull(result);
	}
	
	
	@Test  (expected = Exception.class)
	public void testFullObjectDescription_2()
		throws Exception {
		Set<Object> p = new HashSet();
		p=null;
		FullObjectDescription result = new FullObjectDescription(p);
	}

	@Test
	public void testContainAttribute_1()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		boolean result = fixture.containsAttribute(null);
		assertEquals(false, result);
	}
	
	@Test
	public void testContainAttribute_2()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		boolean result = fixture.containsAttribute("");
		assertEquals(false, result);
	}
	
	@Test
	public void testContainAttribute_3()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		fixture.containsAttribute(1);
		boolean result = fixture.containsAttribute(1);
		assertEquals(false, result);
	}
	
	@Test  (expected = Exception.class)
	public void testContainAttributes_1()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		Set<Object> attrs= new HashSet();
		attrs=null;
		boolean result = fixture.containsAttributes(attrs);
	}
	
	@Test
	public void testContainAttributes_2()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		Set<Object> attrs= new HashSet();
		fixture.containsAttributes(attrs);
		boolean result = fixture.containsAttributes(attrs);
		assertEquals(true, result);
	}
	
	@Test
	public void testAddAttribute_1()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		boolean result = fixture.addAttribute(null);
		assertEquals(true, result);
	}
	
	@Test
	public void testAddAttribute_2()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		fixture.addAttribute("");
		boolean result = fixture.addAttribute("");
		assertEquals(false, result);
	}
	
	@Test
	public void testAddAttribute_3()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		fixture.addAttribute(1);
		boolean result = fixture.addAttribute(1);
		assertEquals(false, result);
	}
	
	@Test
	public void testAddAttributes_1()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		Set<Object> attrs= new HashSet();
		attrs=null;
		boolean result = fixture.addAttribute(attrs);
		assertEquals(true, result);
	}
	
	@Test
	public void testAddAttributes_2()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		Set<Object> attrs= new HashSet();
		fixture.addAttribute(attrs);
		boolean result = fixture.addAttribute(attrs);
		assertEquals(false, result);
	}
	
	
	@Test
	public void testRemoveAttribute_1()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		boolean result = fixture.removeAttribute(null);
		assertEquals(false, result);
	}
	
	@Test
	public void testRemoveAttribute_2()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		boolean result = fixture.removeAttribute("");
		assertEquals(false, result);
	}
	
	@Test
	public void testRemoveAttribute_3()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		fixture.removeAttribute(1);
		fixture.addAttribute(1);
		boolean result = fixture.removeAttribute(1);
		assertEquals(true, result);
	}
	@Test 
	public void testAddAttributes_3()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		Set<Object> attrs= new HashSet();
		attrs.add(1);
		attrs.add(2);
		fixture.addAttributes(attrs);
	}
	
	@Test
	public void testClone()
		throws Exception {
		FullObjectDescription fixture = new FullObjectDescription();
		fixture.clone();
	}
}