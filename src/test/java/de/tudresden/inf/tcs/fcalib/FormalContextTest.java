package de.tudresden.inf.tcs.fcalib;

import java.util.HashSet;
import de.tudresden.inf.tcs.fcaapi.Concept;
import de.tudresden.inf.tcs.fcaapi.Expert;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.test.NoExpertFull;
import java.util.Set;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import org.junit.*;
import static org.junit.Assert.*;


public class FormalContextTest {

	@Test
	public void testFormalContext_1()
		throws Exception {

		FormalContext result = new FormalContext();


		assertNotNull(result);
		assertEquals(null, result.getDuquenneGuiguesBase());
		assertEquals(null, result.allClosures());
		assertEquals(null, result.getIntents());
		assertEquals(null, result.getConceptLattice());
		assertEquals(null, result.getExtents());
		assertEquals(null, result.getConcepts());
		assertEquals(null, result.getExpert());
		assertEquals(null, result.getStemBase());
		assertEquals(0, result.getAttributeCount());
		assertEquals(0, result.getObjectCount());
		assertEquals(null, result.getCurrentQuestion());
		assertEquals(null, result.getImplications());
		assertEquals(false, result.isExpertSet());
	}

	@Test (expected = Exception.class)
	public void testAddAttributeToObject_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));

		boolean result = fixture.addAttributeToObject(null, null);
	}


	@Test (expected = Exception.class)
	public void testAddAttributeToObject_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));

		boolean result = fixture.addAttributeToObject("clothes", null);
		assertTrue(result);
	}


	@Test (expected = Exception.class)
	public void testAddAttributeToObject_3()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));

		boolean result = fixture.addAttributeToObject(null, 1);
		assertTrue(result);
	}

	/**
	 * Run the boolean addAttributeToObject(A,I) method test.
	 */
	@Test (expected = Exception.class)
	public void testAddAttributeToObject_4()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));

		boolean result = fixture.addAttributeToObject("number", 1);
		assertTrue(result);
	}

	/**
	 * Run the boolean addAttributeToObject(A,I) method test.
	 */
	@Test (expected = Exception.class)
	public void testAddAttributeToObject_5()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 0));

		boolean result = fixture.addAttributeToObject("", 0);
		assertTrue(result);
	}

	/**
	 * Run the boolean addObject(FullObject<A,I>) method test.
	 *
	 */
	@Test (expected = Exception.class)
	public void testAddObject_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		FullObject<Object, Object> o = new FullObject((Object) null);
		boolean result = fixture.addObject(o);
	}

	/**
	 * Run the boolean addObject(FullObject<A,I>) method test.
	 *
	 */
	@Test (expected = Exception.class)
	public void testAddObject_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		FullObject<Object, Object> o = new FullObject((Object) 1);

		boolean result = fixture.addObject(o);
		assertTrue(result);
	}

	/**
	 * Run the boolean addObject(FullObject<A,I>) method test.
	 *
	 */
	@Test (expected = Exception.class)
	public void testAddObject_3()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		FullObject<Object, Object> o = new FullObject((Object) "");

		boolean result = fixture.addObject(o);
		assertTrue(result);
	}

	/**
	 * Run the Set<Set<Object>> allClosures() method test.
	 *
	 */
	@Test
	public void testAllClosures_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));

		Set<Set<Object>> result = fixture.allClosures();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the void clearObjects() method test.
	 *
	 */
	@Test
	public void testClearObjects_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));

		fixture.clearObjects();

		// add additional test code here
	}

	/**
	 * Run the Set<Object> closure(Set<A>) method test.
	 *
	 */
	@Test
	public void testClosure_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		Set<Object> x = new HashSet();

		Set<Object> result = fixture.closure(x);
		assertNotNull(result);
		assertEquals(0, result.size());
	}
	
	@Test (expected = Exception.class)
	public void testClosure_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		Set<Object> x = new HashSet();
		x=null;
		Set<Object> result = fixture.closure(x);
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the Set<Object> doublePrime(Set<A>) method test.
	 *
	 */
	@Test
	public void testDoublePrime_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		Set<Object> x = new HashSet();

		Set<Object> result = fixture.doublePrime(x);
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the Set<Object> doublePrime(Set<A>) method test.
	 *
	 */
	@Test (expected = Exception.class)
	public void testDoublePrime_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		Set<Object> x = new HashSet();
        x=null;
		Set<Object> result = fixture.doublePrime(x);
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	/**
	 * Run the Set<Object> doublePrime(Set<A>) method test
	 */
	@Test
	public void testDoublePrime_3()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		Set<Object> x = new HashSet();
		Set<Object> result = fixture.doublePrime(x);
		assertNotNull(result);
		assertEquals(0, result.size());
	}


	@Test
	public void testFollowsFromBackgroundKnowledge_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		FCAImplication<Object> implication = new Implication();

		boolean result = fixture.followsFromBackgroundKnowledge(implication);
		assertEquals(false, result);
	}


	@Test
	public void testGetConceptLattice_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));

		Set<Concept<Object, FullObject<Object, Object>>> result = fixture.getConceptLattice();

		// add additional test code here
		assertEquals(null, result);
	}

	@Test
	public void testGetObjectAtIndex_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		int index = 0;
		FullObject<Object, Object> result = fixture.getObjectAtIndex(index);
		assertNotNull(result);
	}

	
	@Test (expected = java.lang.IndexOutOfBoundsException.class)
	public void testGetObjectAtIndex_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		int index = -1;
		FullObject<Object, Object> result = fixture.getObjectAtIndex(index);
		assertNotNull(result);
	}

	@Test (expected = java.lang.IndexOutOfBoundsException.class)
	public void testGetObjectAtIndex_3() throws IllegalObjectException
		{
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) "car"));
		int index = 1;
		FullObject<Object, Object> result = fixture.getObjectAtIndex(index);
		assertNotNull(result);
	}


	@Test
	public void testGetStemBase_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));

		Set<FCAImplication<Object>> result = fixture.getStemBase();

		// add additional test code here
		assertEquals(null, result);
	}
	
	@Test 
	public void testGetObject_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));

		FullObject<Object, Object> result = fixture.getObject(null);
	    assertEquals(null, result);
	}

	@Test
	public void testGetObject_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));

		FullObject<Object, Object> result = fixture.getObject(1);
	    assertNotNull(result);
	}

	@Test
	public void testGetObject_3()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 0));

		FullObject<Object, Object> result = fixture.getObject(0);
		assertNotNull(result);
	}


	@Test
	public void testObjectHasAttribute_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		FullObject<Object, Object> obj = new FullObject((Object) null);
		boolean result = fixture.objectHasAttribute(obj, null);
		assertEquals(false, result);
	}

	@Test
	public void testObjectHasAttribute_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		FullObject<Object, Object> obj = new FullObject((Object) 1);

		boolean result = fixture.objectHasAttribute(obj, 1);
		assertEquals(false, result);
	}


	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));

		boolean result = fixture.removeAttributeFromObject(null, null);
	}

	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));

		boolean result = fixture.removeAttributeFromObject(1, null);
	}

	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_3()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));

		boolean result = fixture.removeAttributeFromObject(null, 1);
	}

	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_4()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));

		boolean result = fixture.removeAttributeFromObject("", null);

	}

	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_5()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));

		boolean result = fixture.removeAttributeFromObject(1, 1);
		assertTrue(result);
	}
	
	@Test (expected = Exception.class)
	public void testRemoveObject_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		FullObject<Object, Object> o = new FullObject((Object) null);
		o=null;
		boolean result = fixture.removeObject(o);
	}


	@Test
	public void testRemoveObject_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		FullObject<Object, Object> o = new FullObject((Object) null);
		fixture.addObject(o);
		boolean result = fixture.removeObject(o);
		assertTrue(result);
	}

	@Test (expected = Exception.class)
	public void testIsClosed_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		Set<Object> x = new HashSet();
		x=null;
		boolean result = fixture.isClosed(x);

	}

	@Test
	public void testRefutes_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		FCAImplication<Object> imp = new Implication();
		boolean result = fixture.refutes(imp);
		imp=null;
		assertEquals(false, result);
	}

	@Test
	public void testRefutes_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		FCAImplication<Object> imp = new Implication();
		boolean result = fixture.refutes(imp);

		assertEquals(false, result);
	}


	@Test
	public void testRefutes_3()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) "car"));
		FCAImplication<Object> imp = new Implication();

		boolean result = fixture.refutes(imp);
		assertEquals(false, result);
	}

	
	@Test (expected = Exception.class)
	public void testIsCounterExampleValid_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		FullObject<Object, Object> counterExample = new FullObject((Object) null);
		FCAImplication<Object> imp = new Implication();
		counterExample=null;
		imp=null;
		boolean result = fixture.isCounterExampleValid(counterExample, imp);
		assertEquals(false, result);
	}

	@Test
	public void testIsCounterExampleValid_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		FullObject<Object, Object> counterExample = new FullObject((Object) 1);
		FCAImplication<Object> imp = new Implication();

		boolean result = fixture.isCounterExampleValid(counterExample, imp);
		assertEquals(false, result);
	}

	@Test
	public void testSetExpert_1()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		Expert<Object, Object, FullObject<Object, Object>> e = new NoExpertFull(new FormalContext());
		e=null;
		fixture.setExpert(e);

	}
	@Test
	public void testSetExpert_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) null));
		Expert<Object, Object, FullObject<Object, Object>> e = new NoExpertFull(new FormalContext());
		fixture.setExpert(e);

	}

	@Test
	public void testIsClosed_2()
		throws Exception {
		FormalContext fixture = new FormalContext();
		Set<Object> a = new HashSet();
		fixture.isClosed(a);
	}
	@Test
	public void testRemoveObject_3()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		Set<Object> a1 = new HashSet();
		Set<Object> a2 = new HashSet();

		Set<Object> i1 = new HashSet();
		Set<Object> i2 = new HashSet();

		FullObject<Object, Object> o1 = new FullObject(a1,i1);
		FullObject<Object, Object> o2 = new FullObject(a2,i2);

		fixture.addObject(o1);
		fixture.removeObject(o1);
	}
	@Test(expected=Exception.class)
	public void testRemoveObject_4()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		
		Set<Object> i = new HashSet();

		fixture.removeObject(i);
	}
	@Test (expected = Exception.class)
	public void testRemoveAttributeFromObject_6()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		Set<Object> a = new HashSet();
		a.add(1);
		Set<Object> i = new HashSet();
		i.add(1);
		Object a1 = 1;
		FullObject<Object, Object> o = new FullObject(a1,i);
		fixture.addObject(o);
		fixture.removeAttributeFromObject(a1,i);
	}

	@Test
	public void testAddAttributeToObject_6()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		Object a = 1;
		fixture.addAttribute(a);
		fixture.addAttributeToObject(a, 1);
	}
	@Test (expected = Exception.class)
	public void testAddAttributeToObject_7()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		Object a = 1;
		fixture.addAttribute(a);
		fixture.addAttributeToObject(a, 2);
	}
	@Test(expected=Exception.class)
	public void testAddAttributeToObject_8()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		Object a = 1;
		fixture.addAttribute(a);
		fixture.addAttributeToObject(a, 1);
		fixture.addAttributeToObject(a, 1);

	}
	@Test
	public void testRemoveObject()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		fixture.removeObject(1);
	}
	@Test
	public void testRemoveAttributeFromObject_7()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		Object a = 1;
		fixture.addAttribute(a);
		fixture.addAttributeToObject(a, 1);
		fixture.removeAttributeFromObject(a, 1);
		
	}
	@Test(expected=Exception.class)
	public void testRemoveAttributeFromObject_8()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		Object a = 1;
		fixture.addAttribute(a);
		fixture.addAttributeToObject(a, 1);
		fixture.removeAttributeFromObject(a, 2);
		
	}
	@Test(expected= Exception.class)
	public void testRemoveAttributeFromObject_9()
		throws Exception {
		FormalContext fixture = new FormalContext();
		fixture.setExpert(new NoExpertFull(new FormalContext()));
		fixture.addObject(new FullObject((Object) 1));
		Object a = 1;
		Object b = 2;
		fixture.addAttribute(a);
		fixture.addAttribute(b);
		fixture.addAttributeToObject(a, 1);
		fixture.removeAttributeFromObject(b, 1);
		
	}
}