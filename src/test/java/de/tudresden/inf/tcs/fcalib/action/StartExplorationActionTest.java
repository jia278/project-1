package de.tudresden.inf.tcs.fcalib.action;

import java.awt.event.ActionEvent;
import org.junit.*;
import static org.junit.Assert.*;


public class StartExplorationActionTest {

	@Test
	public void testStartExplorationAction_1()
		throws Exception {
		StartExplorationAction result = new StartExplorationAction();
		assertNotNull(result);
	}

	@Test (expected = Exception.class)
	public void testActionPerformed_1()
		throws Exception {
		StartExplorationAction fixture = new StartExplorationAction();
		ActionEvent e = new ActionEvent(new Object(), 1, "");
		e=null;
		fixture.actionPerformed(e);
	}


}