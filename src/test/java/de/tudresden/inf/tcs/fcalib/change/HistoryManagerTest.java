package de.tudresden.inf.tcs.fcalib.change;

import java.util.ArrayList;
import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcaapi.change.ContextChange;
import de.tudresden.inf.tcs.fcalib.Implication;
import static org.junit.Assert.*;


public class HistoryManagerTest {

	@Test
	public void testHistoryManager_1()
		throws Exception {
		HistoryManager result = new HistoryManager();
		assertNotNull(result);
	}


	@Test (expected = Exception.class)
	public void testPush_1()
		throws Exception {
		HistoryManager fixture = new HistoryManager();
		fixture.add(1, (Object) null);
		ContextChange<Object> change = new NewImplicationChange(new FormalContext(), new Implication());
		change=null;
		fixture.push(change);

	}

	@Test (expected = Exception.class)
	public void testPush_2()
		throws Exception {
		HistoryManager fixture = new HistoryManager();
		fixture.add(1, (Object) 1);
		ContextChange<Object> change = new NewImplicationChange(new FormalContext(), new Implication());
		fixture.push(change);

	}
	
	
	@Test (expected = Exception.class)
	public void testUndo_1()
		throws Exception {
		HistoryManager fixture = new HistoryManager();
		fixture.add(1, (Object) null);
		ContextChange<Object> change = new NewImplicationChange(new FormalContext(), new Implication());
		change=null;
		fixture.undo(change);
	}

}