package de.tudresden.inf.tcs.fcalib;

import java.util.HashSet;
import java.util.Set;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import org.junit.*;
import static org.junit.Assert.*;


public class ImplicationSetTest {

	@Test (expected = Exception.class)
	public void testAdd_1()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());
		FCAImplication<Object> imp = new Implication();
		imp=null;
		boolean result = fixture.add(imp);

		assertEquals(true, result);
	}


	@Test
	public void testAdd_2()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());
		FCAImplication<Object> imp = new Implication();

		boolean result = fixture.add(imp);
		assertEquals(true, result);
	}


	@Test
	public void testAllClosures_1()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());

		Set<Set<Object>> result = fixture.allClosures();

		assertNotNull(result);
		assertEquals(1, result.size());
	}


	@Test (expected = Exception.class)
	public void testClosure_1()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());
		Set<Object> x = new HashSet();
		x=null;
		Set<Object> result = fixture.closure(x);

	}


	@Test
	public void testClosure_2()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());
		Set<Object> x = new HashSet();
		Set<Object> result = fixture.closure(x);
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	@Test
	public void testGetContext_1()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());

		AbstractContext<Object, Object, ?> result = fixture.getContext();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.getAttributeCount());
		assertEquals(null, result.getImplications());
		assertEquals(null, result.getCurrentQuestion());
		assertEquals(false, result.isExpertSet());
		assertEquals(null, result.getStemBase());
		assertEquals(0, result.getObjectCount());
	}

	@Test (expected = Exception.class)
	public void testIsClosed_1()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());
		Set<Object> x = new HashSet();
		x=null;
		boolean result = fixture.isClosed(x);

	}


	@Test
	public void testIsClosed_2()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());
		Set<Object> x = new HashSet();
		boolean result = fixture.isClosed(x);
		assertEquals(true, result);
	}


	@Test (expected = Exception.class)
	public void testNextClosure_1()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());
		Set<Object> x = new HashSet();
		x=null;
		Set<Object> result = fixture.nextClosure(x);
		assertEquals(null, result);
	}


	@Test
	public void testNextClosure_2()
		throws Exception {
		ImplicationSet fixture = new ImplicationSet(new FormalContext());
		Set<Object> x = new HashSet();

		Set<Object> result = fixture.nextClosure(x);
		assertEquals(null, result);
	}


}