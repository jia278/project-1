package de.tudresden.inf.tcs.fcalib;

import java.util.HashSet;
import java.util.Set;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import org.junit.*;
import static org.junit.Assert.*;


public class PartialObjectTest {

	@Test
	public void testPartialObject_1()
		throws Exception {
		PartialObject result = new PartialObject(null);
		assertNotNull(result);
		assertEquals("", result.getName());
		assertEquals(null, result.getIdentifier());
	}

	@Test
	public void testPartialObject_2()
		throws Exception {
		Set<Object> attrs = new HashSet();

		PartialObject result = new PartialObject(null, attrs);
		assertNotNull(result);
		assertEquals("{id=null plus=[] minus=[]}", result.toString());
		assertEquals("", result.getName());
		assertEquals(null, result.getIdentifier());
	}


	@Test
	public void testPartialObject_3()
		throws Exception {
		Set<Object> attrs = new HashSet();
		Set<Object> negatedAttrs = new HashSet();

		PartialObject result = new PartialObject(null, attrs, negatedAttrs);
		assertNotNull(result);
		assertEquals("{id=null plus=[] minus=[]}", result.toString());
		assertEquals("", result.getName());
		assertEquals(null, result.getIdentifier());
	}


	@Test
	public void testGetDescription_1()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");

		PartialObjectDescription<Object> result = fixture.getDescription();
		assertNotNull(result);
		assertEquals("plus=[] minus=[]", result.toString());
	}


	@Test
	public void testGetIdentifier_1()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");

		Object result = fixture.getIdentifier();
		assertEquals(null, result);
	}


	@Test
	public void testGetName_1()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");

		String result = fixture.getName();

		// add additional test code here
		assertEquals("", result);
	}


	@Test
	public void testRefutes_1()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");
		FCAImplication<Object> imp = new Implication();
		boolean result = fixture.refutes(imp);
		assertEquals(false, result);
	}

	@Test
	public void testRefutes_2()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) 1, new HashSet(), new HashSet());
		fixture.setName("1");
		FCAImplication<Object> imp = new Implication();
		boolean result = fixture.refutes(imp);
		assertEquals(false, result);
	}


	@Test
	public void testRespects_1()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");
		FCAImplication<Object> imp = new Implication();
		boolean result = fixture.respects(imp);
		assertEquals(true, result);
	}


	@Test (expected = Exception.class)
	public void testRespects_2()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");
		FCAImplication<Object> imp = new Implication();
		imp=null;
		boolean result = fixture.respects(imp);
	}


	@Test 
	public void testSetName_1()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");
		String n = null;
		fixture.setName(n);
	}

	@Test
	public void testSetName_2()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");
		String n = "";
		fixture.setName(n);
	}
	@Test
	public void testSetName_3()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");
		String n = "elephant";
		fixture.setName(n);
	}

	@Test
	public void testToString_1()
		throws Exception {
		PartialObject fixture = new PartialObject((Object) null, new HashSet(), new HashSet());
		fixture.setName("");

		String result = fixture.toString();
		assertEquals("{id=null plus=[] minus=[]}", result);
	}

}