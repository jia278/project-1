package de.tudresden.inf.tcs.fcalib.action;

import java.awt.event.ActionEvent;
import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import static org.junit.Assert.*;


public class CounterExampleProvidedActionTest {

	@Test
	public void testCounterExampleProvidedAction_1()
		throws Exception {
		AbstractContext c = new FormalContext();
		FCAImplication<Object> q = new Implication();

		CounterExampleProvidedAction result = new CounterExampleProvidedAction(c, q, null);

		assertNotNull(result);
		assertEquals(true, result.isEnabled());
	}


	@Test (expected = Exception.class)
	public void testActionPerformed_1()
		throws Exception {
		CounterExampleProvidedAction fixture = new CounterExampleProvidedAction(new FormalContext(), new Implication(), (FCAObject) null);
		ActionEvent e = new ActionEvent(new Object(), 1, "");
		e=null;
		fixture.actionPerformed(e);
	}

	@Test (expected = Exception.class)
	public void testActionPerformed_2()
		throws Exception {
		CounterExampleProvidedAction fixture = new CounterExampleProvidedAction(new FormalContext(), new Implication(), (FCAObject) null);
		ActionEvent e = new ActionEvent(new Object(), 1, "");
		fixture.getContext().isCounterExampleValid(null, null);
		fixture.actionPerformed(e);

	}


	@Test (expected = Exception.class)
	public void testActionPerformed_3()
		throws Exception {
		CounterExampleProvidedAction fixture = new CounterExampleProvidedAction(new FormalContext(), new Implication(), (FCAObject) null);
		ActionEvent e = new ActionEvent(new Object(), 1, "");

		fixture.actionPerformed(e);
	}

}