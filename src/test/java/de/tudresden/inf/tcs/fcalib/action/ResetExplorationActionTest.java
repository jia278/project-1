package de.tudresden.inf.tcs.fcalib.action;

import java.awt.event.ActionEvent;
import org.junit.*;

import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.action.AbstractExpertAction;
import org.apache.log4j.Logger;

import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.action.AbstractExpertAction;

import static org.junit.Assert.*;

public class ResetExplorationActionTest {
	@Test(expected = Exception.class)
	public void testActionPerformed()
		throws Exception {
		ResetExplorationAction fixture = new ResetExplorationAction(null);
		ActionEvent e = new ActionEvent(new Object(), 1, "");
		fixture.actionPerformed(e);

	}

}