package de.tudresden.inf.tcs.fcalib;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.HashSet;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.FCAObject;

import java.util.Set;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import de.tudresden.inf.tcs.fcalib.action.ChangeAttributeOrderAction;
import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;


public class AbstractContextTest {


	@Test (expected = Exception.class)
	public void Abstrac()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		boolean result = fixture.addAttribute(null);
	}
	
	@Test
	public void testAddAttribute_0()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		boolean result = fixture.addAttribute(1);
		assertTrue(result);
	}
	@Test (expected = Exception.class)
	public void testAddAttribute_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		boolean result = fixture.addAttribute(null);
		assertTrue(result);
	}
	@Test
	public void testAddAttribute_3()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		boolean result = fixture.addAttribute(1);
		assertTrue(result);
	}

	@Test
	public void testAddAttribute_4()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		boolean result = fixture.addAttribute("Car");
		assertTrue(result);
	}

	@Test (expected = Exception.class)
	public void testAddAttribute_5()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		boolean result = fixture.addAttribute(1);
		boolean resultAdded= fixture.addAttribute(1);
		assertTrue(resultAdded);
	}

	@Test
	public void testAddAttributes_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		Set<Object> attrs = new HashSet();

		boolean result = fixture.addAttributes(attrs);
		assertEquals(true, result);
	}
	
	@Test (expected = Exception.class)
	public void testAddAttributes_2()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		Set<Object> attrs = new HashSet();
		attrs=null;
		boolean result = fixture.addAttributes(attrs);
		assertEquals(true, result);
	}
	
	@Test
	public void testAddObjects_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		Set<de.tudresden.inf.tcs.fcaapi.FCAObject> s = new HashSet();

		boolean result = fixture.addObjects(s);
		assertEquals(true, result);
	}

	@Test (expected = Exception.class)
	public void testAddObjects_2()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		Set<de.tudresden.inf.tcs.fcaapi.FCAObject> s = new HashSet();
        s=null;
		boolean result = fixture.addObjects(s);
		assertEquals(false, result);
	}
	
	
	@Test
	public void testClearObjects_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		fixture.clearObjects();
		boolean result = fixture.containsObject(null);
		assertEquals(false, result);
	}

	@Test
	public void testContainsObject_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		boolean result = fixture.containsObject(null);
		assertEquals(false, result);
	}


	@Test
	public void testContainsObject_2()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		boolean result = fixture.containsObject("car");
		assertEquals(false, result);
	}


	@Test
	public void testGetAttributeCount_()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		int result = fixture.getAttributeCount();
		assertEquals(0, result);
	}


	@Test
	public void testGetAttributes_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		IndexedSet<Object> result = fixture.getAttributes();

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.size());
	}


	@Test
	public void testGetCurrentQuestion_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		FCAImplication<Object> result = fixture.getCurrentQuestion();

		// add additional test code here
		assertEquals(null, result);
	}


	@Test
	public void testGetImplications_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		Set<FCAImplication<Object>> result = fixture.getImplications();
		assertEquals(null, result);
	}

	@Test
	public void testGetObjectCount_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		int result = fixture.getObjectCount();
		assertEquals(0, result);
	}


	@Test
	public void testInitializeExploration_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		fixture.initializeExploration();

		// add additional test code here
	}

	@Test
	public void testIsExpertSet_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		boolean result = fixture.isExpertSet();
		assertEquals(false, result);
	}

	@Test
	public void testIsExpertSet_2()
		throws Exception {
		AbstractContext fixture = new FormalContext();

		boolean result = fixture.isExpertSet();

		// add additional test code here
		assertEquals(false, result);
	}

	@Test
	public void testSetCurrentQuestion_1()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		FCAImplication<Object> imp = new Implication();

		fixture.setCurrentQuestion(imp);

		// add additional test code here
	}
	@Test(expected = Exception.class)
	public void testContinueExploration() 
		throws Exception {
		AbstractContext fixture = new FormalContext();
		Set<Object> a = null;
		fixture.continueExploration(a);
	}
	@Test
	public void testAddAttributes_3()
		throws Exception {
		AbstractContext fixture = new FormalContext();
		Set<Object> attrs = new HashSet();
		attrs.add(1);
		attrs.add(2);

		boolean result = fixture.addAttributes(attrs);
		assertEquals(true, result);
	}
	@Test(expected = Exception.class)
	public void testContinueExploration_2() 
		throws Exception {
		AbstractContext fixture = new FormalContext();
		Set<Object> a = new HashSet();
		a.add(1);
		a.add(2);
		fixture.continueExploration(a);
	}
	
}
