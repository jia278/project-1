package de.tudresden.inf.tcs.fcalib.action;

import java.awt.event.ActionEvent;
import org.junit.*;
import static org.junit.Assert.*;


public class StopExplorationActionTest {

	@Test
	public void testStopExplorationAction_1()
		throws Exception {
		StopExplorationAction result = new StopExplorationAction();
		assertNotNull(result);
	}


	@Test 
	public void testActionPerformed_1()
		throws Exception {
		StopExplorationAction fixture = new StopExplorationAction();
		ActionEvent e = new ActionEvent(new Object(), 1, "");
		e=null;
		fixture.actionPerformed(e);

	}


}