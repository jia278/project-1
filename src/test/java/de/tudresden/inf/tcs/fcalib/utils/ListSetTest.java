package de.tudresden.inf.tcs.fcalib.utils;

import java.util.ArrayList;
import java.util.Collection;
import org.junit.*;
import static org.junit.Assert.*;

public class ListSetTest {

	@Test
	public void testListSet_1()
		throws Exception {
		Collection<? extends Object> c = new ArrayList();

		ListSet result = new ListSet(c);
		c=null;
		assertNotNull(result);
		assertEquals(0, result.size());
	}
	
	@Test
	public void testListSet_2()
		throws Exception {
		Collection<? extends Object> c = new ArrayList();

		ListSet result = new ListSet(c);
		assertNotNull(result);
		assertEquals(0, result.size());
	}
	
	@Test (expected = Exception.class)
	public void testAdd_1()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();

		ListSet result = new ListSet(o);
		o=null;
		assertNotNull(result.add(o));
		assertEquals(0, result.size());
	}
	
	@Test
	public void testAdd_2()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
		ListSet result = new ListSet(o);
		assertNotNull(result.add(o));
		assertEquals(1, result.size());
	}
	
	@Test (expected = Exception.class)
	public void testAddAll_1()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
        ListSet result = new ListSet(o);
		
		o=null;
		result.add(o);
		assertNotNull(result.addAll(o));
		assertEquals(0, result.size());
	}
	
	@Test
	public void testAddAll_2()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
		ListSet result = new ListSet(o);
		result.add(o);
		assertEquals(false,result.addAll(o));
	}
	
	
	@Test (expected = Exception.class)
	public void testContains_1()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
        ListSet result = new ListSet(o);
		o=null;
		assertNotNull(result.contains(o));
	}
	
	@Test
	public void testContains_2()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
		ListSet result = new ListSet(o);
		result.add(o);
		assertEquals(true,result.contains(o));
	}
	
	@Test (expected = Exception.class)
	public void testContainsAll_1()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
        ListSet result = new ListSet(o);
		o=null;
		assertNotNull(result.containsAll(o));
	}
	
	@Test
	public void testContainsAll_2()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
		ListSet result = new ListSet(o);
		result.add(o);
		assertEquals(true,result.containsAll(o));
	}
	
	@Test (expected = Exception.class)
	public void testEquals_1()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
        ListSet result = new ListSet(o);
        o=null;
        result.add(o);
		assertNotNull(result.equals(o));
	}
	
	@Test
	public void testEquals_2()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
		ListSet result = new ListSet(o);
		result.add(o);
		assertEquals(false,result.equals(o));
	}
	
	@Test (expected = Exception.class)
	public void testRemove_1()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
        ListSet result = new ListSet(o);
        o=null;
        result.remove(o);
	}
	
	@Test
	public void testRemove_2()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
		ListSet result = new ListSet(o);
		result.add(o);
		assertEquals(true,result.remove(o));
	}
	
	@Test
	public void testRemove_3()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
		Collection<? extends Object> c = new ArrayList();
		ListSet result = new ListSet(o);
		ListSet result2 = new ListSet(c);
		result.add(c);
		assertEquals(false,result2.remove(o));
	}
	
	@Test (expected = Exception.class)
	public void testRemoveAll_1()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
        ListSet result = new ListSet(o);
        o=null;
        result.removeAll(o);
	}
	
	@Test
	public void testRemoveAll_2()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
		ListSet result = new ListSet(o);
		result.add(o);
		result.remove(o);
		assertEquals(false,result.removeAll(o));
	}
	
	@Test
	public void testRetainAll_1()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
        ListSet result = new ListSet(o);
        o=null;
        result.retainAll(o);
        assertEquals(false,result.retainAll(o));
	}
	
	@Test
	public void testRetainAll_2()
		throws Exception {
		Collection<? extends Object> o = new ArrayList();
		ListSet result = new ListSet(o);
		result.add(o);
		result.remove(o);
		assertEquals(false,result.retainAll(o));
	}
	
	@Test
	public void testToArray_1()
		throws Exception {
		Collection<? extends Object> c = new ArrayList();

		ListSet result = new ListSet(c);
		c=null;
		assertNotNull(result.toArray());
	}
	
	@Test
	public void testToArray_2()
		throws Exception {
		Collection<? extends Object> c = new ArrayList();

		ListSet result = new ListSet(c);
		assertNotNull(result.toArray());
	}
	
	@Test
	public void testGetIndexOf_1()
		throws Exception {
		Collection<? extends Object> e = new ArrayList();
        ListSet result = new ListSet(e);
        e=null;
        result.getIndexOf(e);
		assertEquals(-1,result.getIndexOf(e));
	}
	
	@Test
	public void testGetIndexOf_2()
		throws Exception {
		Collection<? extends Object> e = new ArrayList();
        ListSet result = new ListSet(e);
		result.add(e);
        result.getIndexOf(e);
		assertEquals(0,result.getIndexOf(e));
	}
	
	@Test
	public void testGetIndexOf_3()
		throws Exception {
		Collection<? extends Object> e = new ArrayList();
		Collection<? extends Object> c = new ArrayList();
		ListSet result = new ListSet(e);
		ListSet result2 = new ListSet(c);
		result.add(c);
		assertEquals(-1,result2.getIndexOf(e));
	}
	
	@Test (expected = Exception.class)
	public void testGetElementAt_1()
		throws Exception {
		Collection<? extends Object> e = new ArrayList();
        ListSet result = new ListSet(e);
        e=null;
        result.getElementAt(-1);
	}
	
	@Test
	public void testGetElementAt_2()
		throws Exception {
		Collection<? extends Object> e = new ArrayList();
        ListSet result = new ListSet(e);
        result.add(e);
        result.getElementAt(0);
		assertNotNull(result.getElementAt(0));
	}
	
	@Test (expected = Exception.class)
	public void testGetElementAt_3()
		throws Exception {
		Collection<? extends Object> e = new ArrayList();
		ListSet result = new ListSet(e);
        result.add(e);
        result.getElementAt(1);
	}
	
}