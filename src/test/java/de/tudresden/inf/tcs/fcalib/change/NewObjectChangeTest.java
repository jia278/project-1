package de.tudresden.inf.tcs.fcalib.change;

import org.junit.*;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import static org.junit.Assert.*;


public class NewObjectChangeTest {

	@Test
	public void testNewObjectChange_1()
		throws Exception {
		Context<Object, Object, FCAObject<Object, Object>> c = new FormalContext();
		FCAObject<Object, Object> o = new FullObject((Object) null);

		NewObjectChange result = new NewObjectChange(c, o);
		c=null;
		o=null;
		
		assertNotNull(result);
		assertEquals(2, result.getType());
	}

	@Test
	public void testNewObjectChange_2()
		throws Exception {
		Context<Object, Object, FCAObject<Object, Object>> c = new FormalContext();
		FCAObject<Object, Object> o = new FullObject((Object) null);

		NewObjectChange result = new NewObjectChange(c, o);
		assertNotNull(result);
		assertEquals(2, result.getType());
	}
	
	@Test
	public void testGetObject_1()
		throws Exception {
		NewObjectChange fixture = new NewObjectChange(new FormalContext(), new FullObject((Object) null));

		FCAObject<Object, Object> result = fixture.getObject();
		assertNotNull(result);
		assertEquals("", result.getName());
		assertEquals(null, result.getIdentifier());
	}


	@Test
	public void testGetType_1()
		throws Exception {
		NewObjectChange fixture = new NewObjectChange(new FormalContext(), new FullObject((Object) null));

		int result = fixture.getType();
		assertEquals(2, result);
	}


}