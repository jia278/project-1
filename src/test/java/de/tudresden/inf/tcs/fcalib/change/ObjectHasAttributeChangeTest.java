package de.tudresden.inf.tcs.fcalib.change;

import org.junit.*;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import static org.junit.Assert.*;

public class ObjectHasAttributeChangeTest {

	@Test
	public void testObjectHasAttributeChange_1()
		throws Exception {
		FCAObject<Object, Object> o = new FullObject((Object) null);

		ObjectHasAttributeChange result = new ObjectHasAttributeChange(o, null);
		o=null;
		
		assertNotNull(result);
		assertEquals(0, result.getType());
		assertEquals(null, result.getAttribute());
	}

	@Test
	public void testObjectHasAttributeChange_2()
		throws Exception {
		FCAObject<Object, Object> o = new FullObject((Object) null);

		ObjectHasAttributeChange result = new ObjectHasAttributeChange(o, 1);
		
		assertNotNull(result.getAttribute());
		assertEquals(0, result.getType());
	}
	
	@Test
	public void testGetAttribute_1()
		throws Exception {
		ObjectHasAttributeChange fixture = new ObjectHasAttributeChange(new FullObject((Object) null), (Object) null);

		Object result = fixture.getAttribute();
		assertEquals(null, result);
	}

	@Test
	public void testGetObject_1()
		throws Exception {
		ObjectHasAttributeChange fixture = new ObjectHasAttributeChange(new FullObject((Object) null), (Object) null);

		FCAObject<Object, Object> result = fixture.getObject();

		assertNotNull(result);
		assertEquals("", result.getName());
		assertEquals(null, result.getIdentifier());
	}


	@Test
	public void testGetType_1()
		throws Exception {
		ObjectHasAttributeChange fixture = new ObjectHasAttributeChange(new FullObject((Object) null), (Object) null);

		int result = fixture.getType();
		assertEquals(0, result);
	}

}