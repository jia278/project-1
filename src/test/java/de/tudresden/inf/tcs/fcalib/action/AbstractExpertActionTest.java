package de.tudresden.inf.tcs.fcalib.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.Implication;

import static org.junit.Assert.*;


public class AbstractExpertActionTest {

	@Test
	public void testSetContext_1()
		throws Exception {
		AbstractExpertAction fixture = new ChangeAttributeOrderAction();
		AbstractContext c = new FormalContext();
		c=null;
		fixture.setContext(c);
	}
	
	@Test
	public void testSetContext_2()
		throws Exception {
		AbstractExpertAction fixture = new ChangeAttributeOrderAction();
		AbstractContext c = new FormalContext();
		fixture.setContext(c);
	}
	
	@Test (expected = Exception.class)
	public void testActionPerformed_1()
		throws Exception {
		AbstractExpertAction fixture = new ChangeAttributeOrderAction();
		AbstractContext c = new FormalContext();
		ActionEvent e = new ActionEvent(new Object(), 1, "");
		e=null;
		fixture.actionPerformed(e);
	}

}