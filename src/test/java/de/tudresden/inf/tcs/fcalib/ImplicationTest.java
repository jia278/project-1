package de.tudresden.inf.tcs.fcalib;

import java.util.HashSet;
import java.util.Set;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import org.junit.*;
import static org.junit.Assert.*;


public class ImplicationTest {

	@Test 
	public void testImplication_1()
		throws Exception {
		
		Set<Object> p = new HashSet();
		Set<Object> c = new HashSet();
		p=null;
		c=null;
		
		Implication result = new Implication(p, c);
		assertNotNull(result);
	}


	@Test
	public void testImplication_2()
		throws Exception {
		
		Set<Object> p = new HashSet();
		Set<Object> c = new HashSet();

		Implication result = new Implication(p, c);
		assertNotNull(result);
	}

	@Test (expected = Exception.class)
	public void testEquals_1()
		throws Exception {
		Implication fixture = new Implication(new HashSet(), new HashSet());
		FCAImplication<Object> imp = new Implication();
		imp=null;
		boolean result = fixture.equals(imp);
	}
	
	@Test
	public void testEquals_2()
		throws Exception {
		Implication fixture = new Implication(new HashSet(), new HashSet());
		FCAImplication<Object> imp = new Implication();
		boolean result = fixture.equals(imp);
		assertEquals(true, result);
	}

	@Test
	public void testGetConclusion_1()
		throws Exception {
		Implication fixture = new Implication(new HashSet(), new HashSet());

		Set<Object> result = fixture.getConclusion();
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	@Test
	public void testGetPremise_1()
		throws Exception {
		Implication fixture = new Implication(new HashSet(), new HashSet());

		Set<Object> result = fixture.getPremise();

		assertNotNull(result);
		assertEquals(0, result.size());
	}


	@Test
	public void testToString_1()
		throws Exception {
		Implication fixture = new Implication(new HashSet(), new HashSet());

		String result = fixture.toString();
		assertEquals("[] -> []", result);
	}



}