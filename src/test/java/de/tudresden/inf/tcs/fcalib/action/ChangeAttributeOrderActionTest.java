package de.tudresden.inf.tcs.fcalib.action;

import java.awt.event.ActionEvent;
import org.junit.*;
import static org.junit.Assert.*;


public class ChangeAttributeOrderActionTest {

	@Test
	public void testChangeAttributeOrderAction_1()
		throws Exception {
		ChangeAttributeOrderAction result = new ChangeAttributeOrderAction();
		assertNotNull(result);

	}


	
	@Test (expected = Exception.class)
	public void testActionPerformed_1()
		throws Exception {
		ChangeAttributeOrderAction fixture = new ChangeAttributeOrderAction();
		ActionEvent e = new ActionEvent(new Object(), 1,"");
		e=null;
		fixture.actionPerformed(e);

	}
}