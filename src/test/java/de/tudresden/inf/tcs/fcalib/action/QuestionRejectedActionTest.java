package de.tudresden.inf.tcs.fcalib.action;

import java.awt.event.ActionEvent;
import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.Implication;
import static org.junit.Assert.*;

public class QuestionRejectedActionTest {

	@Test
	public void testQuestionRejectedAction_1()
		throws Exception {
		QuestionRejectedAction result = new QuestionRejectedAction();
		assertNotNull(result);
	}


	@Test (expected = Exception.class)
	public void testActionPerformed_1()
		throws Exception {
		QuestionRejectedAction fixture = new QuestionRejectedAction();
		fixture.setQuestion(new Implication());
		ActionEvent e = new ActionEvent(new Object(), 1, "");
		e=null;
		fixture.actionPerformed(e);

		
	}


	@Test 
	public void testGetQuestion_1()
		throws Exception {
		QuestionRejectedAction fixture = new QuestionRejectedAction();
		fixture.setQuestion(new Implication());
		FCAImplication<Object> result = fixture.getQuestion();
		assertNotNull(result);
	}

	@Test
	public void testSetQuestion_1()
		throws Exception {
		QuestionRejectedAction fixture = new QuestionRejectedAction();
		fixture.setQuestion(new Implication());
		FCAImplication<Object> q = new Implication();
		q=null;
		fixture.setQuestion(q);
	}
	
	@Test
	public void testSetQuestion_2()
		throws Exception {
		QuestionRejectedAction fixture = new QuestionRejectedAction();
		fixture.setQuestion(new Implication());
		FCAImplication<Object> q = new Implication();
		fixture.setQuestion(q);
	}

}