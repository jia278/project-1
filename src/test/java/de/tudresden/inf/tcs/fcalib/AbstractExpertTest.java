package de.tudresden.inf.tcs.fcalib;

import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcaapi.action.ExpertActionListener;
import de.tudresden.inf.tcs.fcalib.test.NoExpertFull;
import de.tudresden.inf.tcs.fcalib.action.ChangeAttributeOrderAction;
import static org.junit.Assert.*;

public class AbstractExpertTest {

	@Test
	public void testAddExpertActionListener_1()
		throws Exception {
		NoExpertFull fixture = new NoExpertFull(new FormalContext());
		fixture.addExpertActionListener(new FormalContext());
		ExpertActionListener<Object, Object> listener = new FormalContext();
		listener=null;
		fixture.addExpertActionListener(listener);
	}


	@Test (expected = Exception.class)
	public void testFireExpertAction_1()
		throws Exception {
		NoExpertFull fixture = new NoExpertFull(new FormalContext());
		fixture.addExpertActionListener(new FormalContext());
		ExpertAction action = new ChangeAttributeOrderAction();
        action=null;
		fixture.fireExpertAction(action);
	}

	@Test (expected = Exception.class)
	public void testFireExpertAction_2()
		throws Exception {
		NoExpertFull fixture = new NoExpertFull(new FormalContext());
		fixture.addExpertActionListener(new FormalContext());
		ExpertAction action = new ChangeAttributeOrderAction();
		fixture.fireExpertAction(action);
	}
}