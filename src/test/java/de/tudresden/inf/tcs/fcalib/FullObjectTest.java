package de.tudresden.inf.tcs.fcalib;

import java.util.HashSet;
import java.util.Set;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import org.junit.*;
import static org.junit.Assert.*;


public class FullObjectTest {

	@Test
	public void testFullObject_1_1()
		throws Exception {

		FullObject result = new FullObject(null);
		assertNotNull(result);
		assertEquals("{id: null attributes: []}", result.toString());
		assertEquals("", result.getName());
		assertEquals(null, result.getIdentifier());
	}

	
	@Test
	public void testFullObject_1_2()
		throws Exception {

		FullObject result = new FullObject(1);
		assertNotNull(result);
		assertEquals("", result.getName());
		assertEquals(1, result.getIdentifier());
	}
	
	@Test
	public void testFullObject_1_3()
		throws Exception {

		FullObject result = new FullObject("");
		assertNotNull(result);
		assertEquals("", result.getName());
		assertEquals("", result.getIdentifier());
	}
	
	@Test
	public void testFullObject_1_4()
		throws Exception {

		FullObject result = new FullObject("animal");
		assertNotNull(result);
		assertEquals("", result.getName());
		assertEquals("animal", result.getIdentifier());
	}
	
	@Test
	public void testFullObject_2_1()
		throws Exception {
		Set<Object> attrs = new HashSet();

		FullObject result = new FullObject(null, attrs);
		assertNotNull(result);
		assertEquals("{id: null attributes: []}", result.toString());
		assertEquals("", result.getName());
		assertEquals(null, result.getIdentifier());
	}

	@Test  (expected = Exception.class)
	public void testFullObject_2_2()
		throws Exception {
		Set<Object> attrs = new HashSet();
		attrs=null;
		FullObject result = new FullObject(null, attrs);
		assertNotNull(result);
		assertEquals("{id: null attributes: []}", result.toString());
		assertEquals("", result.getName());
		assertEquals(null, result.getIdentifier());
	}
	@Test
	public void testGetDescription_1()
		throws Exception {
		FullObject fixture = new FullObject((Object) null);
		fixture.setName("");

		FullObjectDescription<Object> result = fixture.getDescription();
		assertNotNull(result);
	}

	@Test
	public void testGetIdentifier_1()
		throws Exception {
		FullObject fixture = new FullObject((Object) null);
		fixture.setName("");

		Object result = fixture.getIdentifier();
		assertEquals(null, result);
	}

	@Test
	public void testGetName_1()
		throws Exception {
		FullObject fixture = new FullObject((Object) null);
		fixture.setName("");

		String result = fixture.getName();
		assertEquals("", result);
	}

	@Test (expected = Exception.class)
	public void testRefutes_1()
		throws Exception {
		FullObject fixture = new FullObject((Object) null);
		fixture.setName("");
		FCAImplication<Object> imp = new Implication();
		imp=null;
		boolean result = fixture.refutes(imp);
		assertEquals(false, result);
	}

	@Test
	public void testRefutes_2()
		throws Exception {
		FullObject fixture = new FullObject((Object) 1);
		fixture.setName("");
		FCAImplication<Object> imp = new Implication();
		boolean result = fixture.refutes(imp);
		assertEquals(false, result);
	}

	@Test (expected = Exception.class)
	public void testRespects_1()
		throws Exception {
		FullObject fixture = new FullObject((Object) null);
		fixture.setName("");
		FCAImplication<Object> imp = new Implication();
		imp=null;
		boolean result = fixture.respects(imp);
		assertEquals(true, result);
	}

	@Test
	public void testRespects_2()
		throws Exception {
		FullObject fixture = new FullObject((Object) 1);
		fixture.setName("");
		FCAImplication<Object> imp = new Implication();

		boolean result = fixture.respects(imp);
		assertEquals(true, result);
	}


	@Test
	public void testSetName_1()
		throws Exception {
		FullObject fixture = new FullObject((Object) null);
		fixture.setName("");
		String n = "";
	}


	@Test
	public void testToString_1()
		throws Exception {
		FullObject fixture = new FullObject((Object) null);
		fixture.setName("");

		String result = fixture.toString();
		assertEquals("{id: null attributes: []}", result);
	}

}